import numpy as np
import cv2 as cv

img = cv.imread('source.jpg', 0)
ret,thresh1 = cv.threshold(img,120,255,cv.THRESH_BINARY)
cv.imwrite("result_binary.jpg", thresh1)

thresh1 = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv.THRESH_BINARY,11,5)
cv.imwrite("result_adaptive_binary.jpg", thresh1)