import numpy as np
import cv2

# Open the image
# img = np.array(Image.open('source.jpg')).astype(np.uint8)
gray_img = cv2.imread('source.jpg', 0)

# Prewitt Operator
h, w = gray_img.shape
# define filters
horizontal = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])  # s2
vertical = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])  # s1

# define images with 0s
newgradientImage = np.zeros((h, w))

# offset by 1
for i in range(1, h - 1):
    for j in range(1, w - 1):
        horizontalGrad = (horizontal[0, 0] * gray_img[i - 1, j - 1]) + \
                         (horizontal[0, 1] * gray_img[i - 1, j]) + \
                         (horizontal[0, 2] * gray_img[i - 1, j + 1]) + \
                         (horizontal[1, 0] * gray_img[i, j - 1]) + \
                         (horizontal[1, 1] * gray_img[i, j]) + \
                         (horizontal[1, 2] * gray_img[i, j + 1]) + \
                         (horizontal[2, 0] * gray_img[i + 1, j - 1]) + \
                         (horizontal[2, 1] * gray_img[i + 1, j]) + \
                         (horizontal[2, 2] * gray_img[i + 1, j + 1])

        verticalGrad = (vertical[0, 0] * gray_img[i - 1, j - 1]) + \
                       (vertical[0, 1] * gray_img[i - 1, j]) + \
                       (vertical[0, 2] * gray_img[i - 1, j + 1]) + \
                       (vertical[1, 0] * gray_img[i, j - 1]) + \
                       (vertical[1, 1] * gray_img[i, j]) + \
                       (vertical[1, 2] * gray_img[i, j + 1]) + \
                       (vertical[2, 0] * gray_img[i + 1, j - 1]) + \
                       (vertical[2, 1] * gray_img[i + 1, j]) + \
                       (vertical[2, 2] * gray_img[i + 1, j + 1])

        # Edge Magnitude
        mag = np.sqrt(pow(horizontalGrad, 2.0) + pow(verticalGrad, 2.0))
        newgradientImage[i - 1, j - 1] = mag


cv2.imwrite("result.jpg", newgradientImage)




############ OLD BY PIXEL VERSION #################






# import numpy as np
# from numpy import median
# import cv2


# def prewitt(img, rank = 3):
#     height, width = img.shape[:2]
#     vis = np.zeros((height, width, 3), np.uint8)
#     vis[:height, :width,:3] = img[:height, :width, :3]
#     max_r = 0
#     max_g = 0
#     max_b = 0
#     for x in range(1, height-rank):
#         print("Iterations left: " + str(height - x - rank))
#         for y in range(1, width-rank):

#             z1_r = img[x,y][0] / 3
#             z1_g = img[x,y][1] / 3
#             z1_b = img[x,y][2] / 3

#             z2_r = img[x+1,y][0] / 3
#             z2_g = img[x+1,y][1] / 3
#             z2_b = img[x+1,y][2] / 3

#             z3_r = img[x+2,y][0] / 3
#             z3_g = img[x+2,y][1] / 3
#             z3_b= img[x+2,y][2] / 3

#             z4_r = img[x,y+1][0] / 3
#             z4_g = img[x,y+1][1] / 3
#             z4_b = img[x,y+1][2] / 3

#             z5_r = img[x+1,y+1][0] / 3
#             z5_g = img[x+1,y+1][1] / 3
#             z5_b = img[x+1,y+1][2] / 3

#             z6_r = img[x+2,y+1][0] / 3
#             z6_g = img[x+2,y+1][1] / 3
#             z6_b = img[x+2,y+1][2] / 3

#             z7_r = img[x,y+2][0] / 3
#             z7_g = img[x,y+2][1] / 3
#             z7_b = img[x,y+2][2] / 3

#             z8_r = img[x+1,y+2][0] / 3
#             z8_g = img[x+1,y+2][1] / 3
#             z8_b = img[x+1,y+2][2] / 3

#             z9_r = img[x+2,y+2][0] / 3
#             z9_g = img[x+2,y+2][1] / 3
#             z9_b = img[x+2,y+2][2] / 3

#             Gx = (z7_r + z8_r + z9_r) - (z1_r + z2_r + z3_r)
#             Gy = (z3_r + z6_r + z9_r) - (z1_r + z4_r + z7_r)
#             length_r = np.sqrt((Gx * Gx) + (Gy * Gy))
#             if length_r > max_r:
#                 max_r = length_r
#             vis[x+1, y+1][0] = length_r

#             Gx = (z7_g + z8_g + z9_g) - (z1_g + z2_g + z3_g)
#             Gy = (z3_g + z6_g + z9_g) - (z1_g + z4_g + z7_g)
#             length_g = np.sqrt((Gx * Gx) + (Gy * Gy))
#             if length_g > max_g:
#                 max_g = length_g
#             vis[x+1, y+1][1] = length_g

#             Gx = (z7_b + z8_b + z9_b) - (z1_b + z2_b + z3_b)
#             Gy = (z3_b + z6_b + z9_b) - (z1_b + z4_b + z7_b)
#             length_b = np.sqrt((Gx * Gx) + (Gy * Gy))
#             if length_b > max_b:
#                 max_b = length_b
#             vis[x+1, y+1][2] = length_b


#     for x in range(1, height-rank):
#         for y in range(1, width-rank):
#             vis[x+1, y+1][0] = vis[x+1, y+1][0] / max_r * 255
#             vis[x+1, y+1][1] = vis[x+1, y+1][1] / max_g * 255
#             vis[x+1, y+1][2] = vis[x+1, y+1][2] / max_b * 255
#     return vis

# img = cv2.imread('source.jpg')
# vis = prewitt(img)
# cv2.imwrite("result.jpg", vis)