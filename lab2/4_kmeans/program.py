import numpy as np
import cv2

img = cv2.imread('source.jpg')
Z = img.reshape((-1,3))
# convert to np.float32
Z = np.float32(Z)

# define criteria, number of clusters(K) and apply kmeans()
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 20, 0.1)

for K in range(2, 10):
    print("Clustering: " + str(K)+" clusters")
    ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))
    cv2.imwrite("result_" + str(K)+"_means.jpg", res2)
    print("Done: " + str(K)+" clusters")