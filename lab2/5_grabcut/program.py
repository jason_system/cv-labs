import numpy as np
import cv2

image = cv2.imread('source.jpg')

mask = np.zeros(image.shape[:2], np.uint8)

backgroundModel = np.zeros((1, 65), np.float64)
foregroundModel = np.zeros((1, 65), np.float64)


x = 40
y = 60
w = 520
h = 400


rectangle = (x, y, w, h)
start_point = (x, y)
end_point = (w + x, h + y)
color = (255, 0, 0)
thickness = 2
with_rect = cv2.rectangle(image, start_point, end_point, color, thickness)
image = cv2.imread('source.jpg')
cv2.imwrite("result_rect-position.jpg", with_rect)


cv2.grabCut(image, mask, rectangle,
            backgroundModel, foregroundModel,
            3, cv2.GC_INIT_WITH_RECT)



mask2 = np.where((mask == 2)|(mask == 0), 0, 1).astype('uint8')


# The final mask is multiplied with
# the input image to give the segmented image.
image = image * mask2[:, :, np.newaxis]
cv2.imwrite("result.jpg", image)