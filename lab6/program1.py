import numpy as np
import cv2
import glob
from matplotlib import pyplot as plt
# Define the chess board rows and columns
rows = 7
cols = 6

# Set the termination criteria for the corner sub-pixel algorithm
criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 30, 0.001)

# Prepare the object points: (0,0,0), (1,0,0), (2,0,0), ..., (6,5,0). They are the same for all images
objectPoints = np.zeros((rows * cols, 3), np.float32)
objectPoints[:, :2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2)



img1 = cv2.imread('data/left12.jpg')
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
ret, corners = cv2.findChessboardCorners(img1, (rows, cols), None)
corners = cv2.cornerSubPix(img1, corners, (11, 11), (-1, -1), criteria)
pts1 = corners

img2 = cv2.imread('data/right12.jpg')
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
ret, corners = cv2.findChessboardCorners(img2, (rows, cols), None)

corners = cv2.cornerSubPix(img2, corners, (11, 11), (-1, -1), criteria)
pts2 = corners




F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_LMEDS)

# We select only inlier points
pts1 = pts1[mask.ravel()==1]
pts2 = pts2[mask.ravel()==1]


M1 = np.identity(3, np.float32)
M2 = np.identity(3, np.float32)


D1 = np.zeros((5), np.float32)
D2 = np.zeros((5), np.float32)
_, H1, H2 = cv2.stereoRectifyUncalibrated(pts1, pts2, F, img1.shape)

retval, M1i = cv2.invert(M1)
retval, M2i = cv2.invert(M2)
R1, R2 = np.dot(np.dot(M1i, H1), M1), np.dot(np.dot(M2i, H2), M2)
map1x, map1y = cv2.initUndistortRectifyMap(M1, D1, R1, M1, img1.shape, cv2.CV_32FC1)
map2x, map2y = cv2.initUndistortRectifyMap(M2, D2, R2, M2, img2.shape, cv2.CV_32FC1)
img1r = cv2.remap(img1, map1x, map1y, cv2.INTER_LINEAR)
img2r = cv2.remap(img2, map2x, map2y, cv2.INTER_LINEAR)
cv2.imwrite('source-left.jpg', img1)
cv2.imwrite('result-left.jpg', img1r)
cv2.imwrite('source-right.jpg', img2)
cv2.imwrite('result-right.jpg', img2r)

