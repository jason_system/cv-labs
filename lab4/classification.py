import cv2
import numpy as np
from os.path import join
import os

# CMake -DOPENCV_ENABLE_NONFREE:BOOL=ON
# placeholder path
datapath = "PetImages"
def path(cls,i):
    return "%s/%s/%d.jpg"  % (datapath,cls,i+1)

pos, neg = "Cat/", "Dog/"

detect = cv2.ORB_create(nfeatures=2000)
extract = cv2.ORB_create(nfeatures=2000)



flann_params = dict(algorithm = 1, trees = 5)
matcher = cv2.FlannBasedMatcher(flann_params, {})



bow_kmeans_trainer = cv2.BOWKMeansTrainer(100)
# extract_bow = cv2.BOWImgDescriptorExtractor(extract, matcher)
extract_bow = cv2.BOWImgDescriptorExtractor(extract, cv2.BFMatcher(cv2.NORM_HAMMING))


def extract_sift(fn): # returns descriptor
    im = cv2.imread(fn, 0)
    return np.float32(extract.compute(im, detect.detect(im))[1])

for i in range(200, 300):
    # print(os.path.isfile(path(pos,i)))
    bow_kmeans_trainer.add(extract_sift(path(pos,i)))
    bow_kmeans_trainer.add(extract_sift(path(neg,i)))
    print(extract_sift(path(neg,i)).shape)
    print(i)

# print(bow_kmeans_trainer)
print("Please wait")
voc = bow_kmeans_trainer.cluster()
extract_bow.setVocabulary( voc )

print(extract_bow)
def bow_features(fn):
    im = cv2.imread(fn, 0)
    print(extract_bow.compute(im, detect.detect(im)))
    return extract_bow.compute(im, detect.detect(im))

traindata, trainlabels = [], []
for i in range(200, 300):
    # print("add labels " + str(i))
    # print(bow_features(path(pos, i)).shape)
    traindata.extend(bow_features(path(pos, i)))
    trainlabels.append(1)
    traindata.extend(bow_features(path(neg, i)))
    trainlabels.append(-1)

svm = cv2.ml.SVM_create()
svm.train(np.array(traindata), cv2.ml.ROW_SAMPLE, np.array(trainlabels))

def predict(fn):
    f = bow_features(fn);
    p = svm.predict(f)
    print(str(fn) + "\t" + str(p[1][0][0]))
    return p

# again placeholder paths

right = 0

for i in range(150, 200):
    print("test " + str(i))
    cat, dog = path(pos,i), path(neg, i)
    cat_img = cv2.imread(cat)
    dog_img = cv2.imread(dog)
    cat_predict = predict(cat)
    dog_predict = predict(dog)


    if (cat_predict[1][0][0] == 1.0):
        right = right + 1

    if (dog_predict[1][0][0] == -1.0):
        right = right + 1

val_acc = right / 100
print(str(right) + "%")

