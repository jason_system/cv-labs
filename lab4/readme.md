Алгоритм sift.cpp был запатентован и поэтому нужно использовать более раннюю версию python и opencv

Для запуска лабораторной в контейнере

1. Установить docker и docker-compose на компьютер
2. Добавить пользователя в группу докера, выполнить
sudo usermod -aG docker $USER
3. Перезагрузить компьютер

4. Скачать изображения по ссылке https://yadi.sk/d/T0Hbd04jMr6kAQ 
5. Поместить содержимое архива в папку PetImages
6. docker-compose up --build
7. docker-compose run --rm app bash 
8. В контейнере python classification.py
9. Результаты будут отображены в консоли

Для выхода из контейнера 
exit