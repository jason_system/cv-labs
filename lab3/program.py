import cv2
import numpy as np


img = cv2.imread("source_templates.jpg") ## templates
ret, thresh = cv2.threshold(cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY) , 127, 255, cv2.THRESH_BINARY)
contours, hier = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

D = contours[0] # template Д
N = contours[1] # template Н



img = cv2.imread("source.jpg", 3) ## source
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray , 150, 255, cv2.THRESH_BINARY)
contours, hier = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2:]


for c in contours:
    this_score = cv2.matchShapes(c, D, 1, 0.0)
    
    if this_score < 0.1:
        x,y,w,h = cv2.boundingRect(c)
        cv2.rectangle(img, (x,y), (x+w, y+h), (0, 255, 0), 3)

    this_score = cv2.matchShapes(c, N, 1, 0.0)
    if this_score < 1.2:
        (x,y),radius = cv2.minEnclosingCircle(c)
        center = (int(x),int(y))
        radius = int(radius)
        img = cv2.circle(img,center,radius,(0,0,255),3)

cv2.imwrite("result.jpg", img)
