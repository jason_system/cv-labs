git clone https://jason_system@bitbucket.org/jason_system/cv-labs.git

cd cv-labs/

Установка

1. python -m venv env 
2. source env/bin/activate 
3. pip install -r requirements.txt 

 
lab1

1. source env/bin/activate
2. cd lab1/ 
3. python program.py

lab2 (пример prewitt)

1. source env/bin/activate нет необходимости выполнять каждый раз, если консоль уже находится в окружении env 
2. cd 1_prewitt/ 
3. python program.py 

результат будет находиться в файле result.jpg

Остальные во второй лабораторной аналогично


lab3 - как lab2 

lab4 

Инструкция находится в файле readme.txt в папке lab4 

lab5 - как lab2, но вывод происходит как в лабе 1
