import numpy as np
import cv2

cap = cv2.VideoCapture('source.MOV')
MAXIMUM_CORNERS = 8

colors = np.random.randint(0, 255, (100, 3))


# Take first frame and find corners in it
ret, prev_frame = cap.read()
old_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
cv2.imwrite("result_gray.jpg", old_gray)

# ShiTomasi corner detection
p0 = cv2.goodFeaturesToTrack(
    old_gray, 
    mask = None, 
    maxCorners = MAXIMUM_CORNERS,
    qualityLevel = 0.3,
    minDistance = 7,
    blockSize = 7,
)
# for p in p0:
#     prev_frame = cv2.circle(prev_frame, (p[0][0], p[0][1]), 3, (0, 0, 255), -1)
# cv2.imwrite("result_points.jpg", prev_frame)

# # Create a mask image for drawing purposes
mask = np.zeros_like(prev_frame)

# Parameters for lucas kanade optical flow
lukas_kanade_params = {
    'winSize': (15, 15),
    'maxLevel': 2,
    'criteria': (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)
}

while(1):
    ret, frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # cv2.imshow('frame_gray', frame_gray)
    # calculate optical flow
    p1, st, err = cv2.calcOpticalFlowPyrLK(
        old_gray, frame_gray, p0, None, **lukas_kanade_params)

    # Select good points
    good_new = p1[st == 1]
    good_old = p0[st == 1]
    # print(good_old)
    # draw the tracks
    for i, (new, old) in enumerate(zip(good_new, good_old)):
        a, b = np.int32(new.ravel())
        c, d = np.int32(old.ravel())
        mask = cv2.line(mask, (a, b), (c, d), colors[i].tolist(), 2)
        frame = cv2.circle(frame, (a, b), 5, colors[i].tolist(), -1)
    img = cv2.add(frame, mask)

    cv2.imshow('frame', img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1, 1, 2)

cv2.destroyAllWindows()
cap.release()
