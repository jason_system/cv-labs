import cv2 as cv
import numpy as np

video = 'source.mp4'
capture = cv.VideoCapture(cv.samples.findFileOrKeep(video))
if not capture.isOpened:
    print('Unable to open: ' + video)
    exit(0)
_,f = capture.read()
source = np.float32(f)
avg = np.float32(f)
while(1):
    _,f = capture.read()

    cv.accumulateWeighted(f, source, 1) 
    cv.accumulateWeighted(f, avg, 0.001)

    res1 = cv.convertScaleAbs(source)
    res2 = cv.convertScaleAbs(avg)

    
    cv.imshow('Source video', res1)
    cv.imshow('Averaged', res2)
    cv.imshow('Result', cv.absdiff(res1, res2))
    
    # wait key milliseconds
    # To make pause for taking screens
    k = cv.waitKey(10)
    if k == 32:
        while(1):
            k = cv.waitKey(100)
            if k == 32:
                break
    
    # To close program
    k = cv.waitKey(10)
    if k == 27: # key ESC for break while
        break

cv.destroyAllWindows()
cv.release()